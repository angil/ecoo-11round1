import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class s1 {
	public static void main(String[] args) throws IOException {
		Scanner in = new Scanner(new File("DATA11.txt"));
		for (int i = 0; i < 5; i++) {
			String next = in.next();
			
			System.out.print("* ");
			for (int j = 0; j < next.length(); j++) {
				System.out.print(next.charAt(j) + " ");
			}
			System.out.println("*");
			
			for (int j = 0; j < next.length(); j++) {
				System.out.print(next.charAt(next.length() - j - 1) + " ");
				
				for (int j2 = 0; j2 < next.length(); j2++) {
					System.out.print("* ");
				}
				
				System.out.println(next.charAt(j));
			}
			
			System.out.print("* ");
			for (int j = next.length() - 1; j >= 0; j--) {
				System.out.print(next.charAt(j) + " ");
			}
			System.out.println("*");
		}
		in.close();
	}
}