import java.awt.Point;
import java.awt.Polygon;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Scanner;

public class s4edit {
	public static void main(String[] args) throws IOException {
		Scanner in = new Scanner(new File("DATA42.txt"));
		for (int rep = 0; rep < 5; rep++) {
			int n = in.nextInt();
			LinkedList<Point> pts = new LinkedList<Point>();
			for (int i = 0; i < n; i++)
				pts.add(new Point(in.nextInt(), in.nextInt()));
			LinkedList<Point> rpts = new LinkedList<Point>(pts);
			for (int i = 0; i < pts.size(); i++) {
				for (int j = i; j < pts.size(); j++) {
					for (int k = j; k < pts.size(); k++) {
						Polygon p = new Polygon();
						p.addPoint(pts.get(i).x, pts.get(i).y);
						p.addPoint(pts.get(j).x, pts.get(j).y);
						p.addPoint(pts.get(k).x, pts.get(k).y);
						for (int l = 0; l < rpts.size(); l++) {
							if (p.contains(rpts.get(l))
									&& !rpts.get(l).equals(pts.get(i))
									&& !rpts.get(l).equals(pts.get(j))
									&& !rpts.get(l).equals(pts.get(k)))
								rpts.remove(rpts.get(l));
						}
					}
				}
			}
			System.out.printf("There are %d perimeter trees in this grove of %d trees.%n", rpts.size(), n);
		}
		in.close();
	}
}