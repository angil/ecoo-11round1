import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class s4 {
	public static void main(String[] args) throws IOException {
		Scanner in = new Scanner(new File("DATA41.txt"));
		
		for (int rep = 0; rep < 5; rep++) {
			
			int n = in.nextInt();
			ArrayList<Point> pts = new ArrayList<Point>(0);
			ArrayList<Point> polygonPts = new ArrayList<Point>(0);
			
			for (int i = 0; i < n; i++) {
				pts.add(new Point(in.nextInt(), in.nextInt()));
			}
			
			if(n <= 3){
				System.out.printf("There are %d perimeter trees in this grove of %d trees.%n", n, n);
				continue;
			}
			
//			Polygon poly = new Polygon();
			
			int maxx = Integer.MIN_VALUE;
			int maxy = Integer.MIN_VALUE;
			int minx = Integer.MAX_VALUE;
			int miny = Integer.MAX_VALUE;
			for (Point pt : pts) {
				if (pt.x > maxx)
					maxx = pt.x;
				if (pt.x < minx)
					minx = pt.x;
				if (pt.y > maxy)
					maxy = pt.y;
				if (pt.y < miny)
					miny = pt.y;
			}
			
			// ArrayList<Point> t = new ArrayList<Point>();
			
			// insert Jade code here
			// MAKE POLYGON
			/*
			 * for (Point pt : pts) { if (pt.y == maxy) { t.add(pt);
			 * pts.remove((pt)); numTrees = numTrees + 1; } } for (int i =
			 * t.size()-1; i >= 0; i--) { int lx = Integer.MAX_VALUE; for (int j
			 * = 0; j < i; j++) { if (t.get(j).x < lx) { t.add(t.remove(j));
			 * break; } } }
			 * 
			 * 
			 * for (Point pt : pts) { if (pt.x == maxx) { poly.addPoint(pt.x,
			 * pt.y); pts.remove((pt)); numTrees = numTrees + 1; } } for (Point
			 * pt : pts) { if (pt.y == miny) { poly.addPoint(pt.x, pt.y);
			 * pts.remove((pt)); numTrees = numTrees + 1; } } for (Point pt :
			 * pts) { if (pt.x == minx) { poly.addPoint(pt.x, pt.y);
			 * pts.remove((pt)); numTrees = numTrees + 1; } }
			 */

			// make rectangle
			Rectangle rect = new Rectangle(minx, miny, maxx - minx, maxy - miny);
			
			// Clean polygon.
			/*
			 * for (Point pt : pts) { if (poly.contains(pt)) { pts.remove((pt));
			 * } }
			 */

			boolean temp = false;
			while (!pts.isEmpty()) {
				// reduce rect
				
				Point mixu = new Point(rect.x + rect.width, rect.y);
				Point maxu = new Point(rect.x, rect.y);
				Point mixd = new Point(rect.x + rect.width, rect.y
						+ rect.height);
				Point maxd = new Point(rect.x, rect.y + rect.height);
				Point miyl = new Point(rect.x, rect.y + rect.height);
				Point mayl = new Point(rect.x, rect.y);
				Point miyr = new Point(rect.x + rect.width, rect.y
						+ rect.height);
				Point mayr = new Point(rect.x + rect.width, rect.y);
				
				ArrayList<Point> newps = new ArrayList<Point>(); // new polygon
																	// points
				
				// is point on rect?
				for (Point pt : pts) {
					if (pt.x == rect.x) { // left side
						if (pt.y < miyl.y)
							miyl = pt;
						if (pt.y > mayl.y)
							mayl = pt;
						newps.add(pt);
					}
					if (pt.x == rect.x + rect.width) { // right side
						if (pt.y < miyr.y)
							miyr = pt;
						if (pt.y > mayr.y)
							mayr = pt;
						newps.add(pt);
					}
					if (pt.y == rect.y) { // up
						if (pt.x < mixu.x)
							mixu = pt;
						if (pt.x > maxu.x)
							maxu = pt;
						newps.add(pt);
					}
					if (pt.y == rect.y + rect.height) { // down
						if (pt.x < mixd.x)
							mixu = pt;
						if (pt.x > maxd.x)
							maxu = pt;
						newps.add(pt);
					}
				}
				
				if (temp) {
					ArrayList<Point> temppts = new ArrayList<Point>();
					
					// triangles!
					// topleft uses mixu and mayl
					Polygon tl = new Polygon();
					tl.addPoint(mixu.x, mixu.y);
					tl.addPoint(rect.x, rect.y);
					tl.addPoint(mayl.x, mayl.y);
					for (Point p : pts) {
						if (tl.contains(p) && !p.equals(mixu)
								&& !p.equals(mayl))
							temppts.add(p);
					}
					
					// top right uses maxu and mayr
					tl = new Polygon();
					tl.addPoint(maxu.x, maxu.y);
					tl.addPoint(rect.x + rect.width, rect.y);
					tl.addPoint(mayr.x, mayr.y);
					for (Point p : pts) {
						if (tl.contains(p) && !p.equals(maxu)
								&& !p.equals(mayr))
							temppts.add(p);
					}
					
					// lower left uses mixd and miyl
					tl = new Polygon();
					tl.addPoint(mixd.x, mixd.y);
					tl.addPoint(rect.x, rect.y + rect.height);
					tl.addPoint(miyl.x, miyl.y);
					for (Point p : pts) {
						if (tl.contains(p) && !p.equals(mixd)
								&& !p.equals(miyl))
							temppts.add(p);
					}
					
					// lower right uses maxd and miyr
					tl = new Polygon();
					tl.addPoint(maxd.x, maxd.y);
					tl.addPoint(rect.x + rect.width, rect.y + rect.height);
					tl.addPoint(miyr.x, miyr.y);
					for (Point p : pts) {
						if (tl.contains(p) && !p.equals(maxd)
								&& !p.equals(miyr))
							temppts.add(p);
					}
					
					pts = temppts;
				} else {
					temp = true;
					pts.remove(mixu);
					pts.remove(mixd);
					pts.remove(maxu);
					pts.remove(maxd);
					pts.remove(miyl);
					pts.remove(mayl);
					pts.remove(miyr);
					pts.remove(mayr);
				}
				polygonPts.addAll(newps);
				
				// rect.height -= 2;
				// rect.width -= 2;
				// rect.translate(1, 1);
				Rectangle tr = new Rectangle(rect.x + 1, rect.y + 1, rect.height - 2, rect.width - 2);
				rect = tr;
			}
			
			/*
			 * ArrayList<Point> sp = new ArrayList<Point>();
			 * 
			 * double sd = Integer.MAX_VALUE;
			 * 
			 * for (Point pt : pts) { if (pt.distance(maxx, maxy) < sd) { sd =
			 * pt.distance(maxx, maxy); sp.clear(); sp.add(new Point(pt)); }
			 * else if (pt.distance(maxx, maxy) == sd) { sp.add(new Point(pt));
			 * } }
			 * 
			 * if (sp.size() == 1) {
			 * 
			 * } else if (sp.size() >= 2) {
			 * 
			 * }
			 */

			System.out.printf("There are %d perimeter trees in this grove of %d trees.%n", polygonPts.size(), n);
		}
		
		in.close();
	}
}